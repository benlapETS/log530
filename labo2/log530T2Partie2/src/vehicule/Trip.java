
package vehicule;

public class Trip {
  private Vehicule vehicule;
  private int distance;

  public Trip(int distance, Vehicule vehicule) {
    this.vehicule = vehicule;
    this.distance = distance;
  }

  public double getExpectedTripDuration() {
    double speed = vehicule.calculateSpeed();
    return distance / speed;
  }
}
