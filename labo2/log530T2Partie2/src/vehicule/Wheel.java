
package vehicule;

public class Wheel {
  private double circumference;
  private double tireSizeDiameter;

  public Wheel(double tireSizeDiameter) {
    this.tireSizeDiameter = tireSizeDiameter;
    circumference = calculateCircumference(tireSizeDiameter);
  }

  private double calculateCircumference(double tireSizeDiameter) {
    return tireSizeDiameter * Math.PI;
  }

  public double getCircumferenceInMeter() {
    return circumference;
  }

  public double getTireSize() {
    return tireSizeDiameter;
  }

}
