
package vehicule;

public class Transmission {
  public enum Gear {
    FIRST(3.615), SECOND(2.053), THIRD(1.370), FOURTH(1.031), FIFTH(
        0.837), NEUTRAL(0), REVERSE(3.583);

    private double ratio;

    private Gear(double ratio) {
      this.ratio = ratio;
    }

    public double getRatio() {
      return ratio;
    }
  }

  private Gear currentGear;
  private double differentialRatio;

  public Transmission(double differentialRatio) {
    currentGear = Gear.NEUTRAL;
    this.differentialRatio = differentialRatio;
  }

  public void setCurrentGear(Gear gear) {
    this.currentGear = gear;
  }

  public double findWheelRPM(int rpm) {
    return rpm / currentGear.getRatio() / differentialRatio;
  }

}
