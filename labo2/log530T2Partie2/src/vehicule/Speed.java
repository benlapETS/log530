
package vehicule;

public class Speed {
  private double speedInMeterPerMinute;

  public Speed(double wheelRPM, double circumferenceInMeter) {
    speedInMeterPerMinute = convertToLinearSpeed(wheelRPM, circumferenceInMeter);
  }

  private double convertToLinearSpeed(double wheelRPM, double circumferenceInMeter) {
    return wheelRPM * circumferenceInMeter;
  }

  public double getInKmPerHour() {
    return speedInMeterPerMinute * 0.06;
  }

}
