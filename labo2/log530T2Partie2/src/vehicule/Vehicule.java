
package vehicule;

import vehicule.Transmission.Gear;

public class Vehicule {
  private Transmission transmission;
  private int engineRPM;
  private Wheel wheel;

  public Vehicule(double tireSizeDiameter, double differentialRatio) {
    transmission = new Transmission(differentialRatio);
    engineRPM = 0;
    wheel = new Wheel(tireSizeDiameter);
  }

  public Vehicule(double tireSizeDiameter) {
    this(tireSizeDiameter, 4.0);
  }

  public Vehicule() {
    this(0.5);
  }

  public double calculateSpeed() {
    double wheelRPM = transmission.findWheelRPM(engineRPM);
    Speed carSpeed = new Speed(wheelRPM, wheel.getCircumferenceInMeter());
    return carSpeed.getInKmPerHour();
  }

  public void setRpm(int rpm) {
    engineRPM = rpm;
  }

  public void setCurrentGear(Gear gear) {
    transmission.setCurrentGear(gear);
  }

  public double getTireSize() {
    return wheel.getTireSize();
  }
}
