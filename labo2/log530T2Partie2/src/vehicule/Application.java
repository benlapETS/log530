
package vehicule;

import vehicule.Transmission.Gear;

public class Application {
  public static void main(String[] args) {
    Vehicule v = new Vehicule(0.5588, 4.041);
    v.setRpm(3500);
    v.setCurrentGear(Gear.FIFTH);
    Trip t = new Trip(100, v);
    System.out
        .println("Car traveling at : " + "->I need help to get this result<-");
    System.out
        .println("Expected trip duration : " + t.getExpectedTripDuration());
  }
}
