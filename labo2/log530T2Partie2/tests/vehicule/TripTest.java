
package vehicule;

import junit.framework.TestCase;
import vehicule.Transmission.Gear;

public class TripTest extends TestCase {
  private int distance;
  private int rpm;
  private Vehicule v;
  private Vehicule v2;
  private double tireSize;

  @Override
  protected void setUp() throws Exception {
    super.setUp();
    this.distance = 100;
    this.rpm = 3000;
    v = new Vehicule();
    this.tireSize = v.getTireSize();
    v2 = new Vehicule();
  }

  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
  }

  public void testDurationInFirstGear() {
    Trip t1 = createTrip(Gear.FIRST, v);
    assertEquals(generateExpectedResult(3.615), t1.getExpectedTripDuration());
  }

  public void testDurationInFifthGear() {
    Trip t5 = createTrip(Gear.FIFTH, v);
    assertEquals(generateExpectedResult(0.837), t5.getExpectedTripDuration());
  }

  public void testCompareFirstWithFifthGear() {
    Trip t1 = createTrip(Gear.FIRST, v);
    Trip t5 = createTrip(Gear.FIFTH, v2);
    assertTrue(t1.getExpectedTripDuration() < t5.getExpectedTripDuration());
  }

  public void testDurationInNeutral() {
    Trip tn = createTrip(Gear.NEUTRAL, v);
    assertEquals(0.0, tn.getExpectedTripDuration());
  }

  public void testDurationInReverse() {
    Trip tr = createTrip(Gear.REVERSE, v);
    assertEquals(generateExpectedResult(3.583), tr.getExpectedTripDuration());
  }

  private double generateExpectedResult(double gearRatio) {
    return distance / ((tireSize * Math.PI * (rpm / gearRatio) * .06));
  }

  private Trip createTrip(Gear gear, Vehicule v) {
    v.setRpm(rpm);
    v.setCurrentGear(gear);
    return new Trip(distance, v);
  }
}
