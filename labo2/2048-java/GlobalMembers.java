public class GlobalMembers
{
	/*
	 ============================================================================
	 Name        : 2048.c
	 Author      : Maurits van der Schee
	 Description : Console version of the game "2048" for GNU/Linux
	 ============================================================================
	 */


	public static int score = 0;
	public static byte scheme = 0;

	public static void getColor(byte value, String color, int length)
	{
		byte[] original = {8,(byte) 255,1,(byte) 255,2,(byte) 255,3,(byte) 255,4,(byte) 255,5,(byte) 255,6,(byte) 255,7,
				(byte) 255,9,0,10,0,11,0,12,0,13,0,14,0,(byte) 255,0,(byte) 255,0};
		byte[] blackwhite = {(byte) 232,(byte) 255,(byte) 234,(byte) 255,(byte) 236,(byte) 255,(byte) 238,(byte) 255,(byte) 240,(byte) 255,
				(byte) 242,(byte) 255,(byte) 244,(byte) 255,(byte) 246,0,(byte) 248,0,(byte) 249,0,(byte) 250,0,(byte) 251,0,(byte) 252,0,
				(byte) 253,0,(byte) 254,0,(byte) 255,0};
		byte[] bluered = {(byte) 235,(byte) 255,63,(byte) 255,57,(byte) 255,93,(byte) 255,(byte) 129,(byte) 255,(byte) 165,(byte) 255,
				(byte) 201,(byte) 255,(byte) 200,(byte) 255,(byte) 199,(byte) 255,(byte) 198,(byte) 255,(byte) 197,(byte) 255,(byte) 196,(byte) 255,(byte) 196,(byte) 255,
				(byte) 196,(byte) 255,(byte) 196,(byte) 255,(byte) 196,(byte) 255};
		byte[][] schemes = {original,blackwhite,bluered};
//C++ TO JAVA CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
		byte[] background = schemes[scheme];
//C++ TO JAVA CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
		byte[] foreground = schemes[scheme+1];
		if (value > 0)
		{
			while (value-- != 0)
			{
			if (background + 2 < schemes[scheme] + 1)
			{
				background += 2;
				foreground += 2;
			}
			}
		}
		snprintf(color.argValue,length,"\033[38;5;%d;48;5;%dm",*foreground,*background);
	}

	public static void drawBoard(byte[][] board)
	{
		byte x;
		byte y;
		byte c;
		String color = new String(new char[40]);
		String reset = "\033[m";
		System.out.print("\033[H");

		System.out.printf("2048.c %17d pts\n\n",score);

		for (y = 0;y < DefineConstants.SIZE;y++)
		{
			for (x = 0;x < DefineConstants.SIZE;x++)
			{
			tangible.RefObject<String> tempRef_color = new tangible.RefObject<String>(color);
				getColor(board[x][y], tempRef_color, 40);
				color = tempRef_color.argValue;
				System.out.printf("%s",color);
				System.out.print("       ");
				System.out.printf("%s",reset);
			}
			System.out.print("\n");
			for (x = 0;x < DefineConstants.SIZE;x++)
			{
			tangible.RefObject<String> tempRef_color2 = new tangible.RefObject<String>(color);
				getColor(board[x][y], tempRef_color2, 40);
				color = tempRef_color2.argValue;
				System.out.printf("%s",color);
				if (board[x][y] != 0)
				{
					String s = new String(new char[8]);
					snprintf(s,8,"%u",(int)1 << board[x][y]);
					byte t = 7 - s.length();
					System.out.printf("%*s%s%*s",t - t / 2,"",s,t / 2,"");
				}
				else
				{
					System.out.print("   Â·   ");
				}
				System.out.printf("%s",reset);
			}
			System.out.print("\n");
			for (x = 0;x < DefineConstants.SIZE;x++)
			{
			tangible.RefObject<String> tempRef_color3 = new tangible.RefObject<String>(color);
				getColor(board[x][y], tempRef_color3, 40);
				color = tempRef_color3.argValue;
				System.out.printf("%s",color);
				System.out.print("       ");
				System.out.printf("%s",reset);
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		System.out.print("        â†�,â†‘,â†’,â†“ or q        \n");
		System.out.print("\033[A"); // one line up
	}

	public static byte findTarget(byte[] array, byte x, byte stop)
	{
		byte t;
		// if the position is already on the first, don't evaluate
		if (x == 0)
		{
			return x;
		}
		for (t = x - 1;t >= 0;t--)
		{
			if (array[t] != 0)
			{
				if (array[t] != array[x])
				{
					// merge is not possible, take next position
					return t + 1;
				}
				return t;
			}
			else
			{
				// we should not slide further, return this one
				if (t == stop)
				{
					return t;
				}
			}
		}
		// we did not find a
		return x;
	}

	public static boolean slideArray(byte[] array)
	{
		boolean success = false;
		byte x;
		byte t;
		byte stop = 0;

		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			if (array[x] != 0)
			{
				t = findTarget(array, x, stop);
				// if target is not original position, then move or merge
				if (t != x)
				{
					// if target is zero, this is a move
					if (array[t] == 0)
					{
						array[t] = array[x];
					}
					else if (array[t] == array[x])
					{
						// merge (increase power of two)
						array[t]++;
						// increase score
						score += (int)1 << array[t];
						// set stop to avoid double merge
						stop = t + 1;
					}
					array[x] = 0;
					success = true;
				}
			}
		}
		return success;
	}

	public static void rotateBoard(byte[][] board)
	{
		byte i;
		byte j;
		byte n = DefineConstants.SIZE;
		byte tmp;
		for (i = 0; i < n / 2; i++)
		{
			for (j = i; j < n - i - 1; j++)
			{
				tmp = board[i][j];
				board[i][j] = board[j][n - i - 1];
				board[j][n - i - 1] = board[n - i - 1][n - j - 1];
				board[n - i - 1][n - j - 1] = board[n - j - 1][i];
				board[n - j - 1][i] = tmp;
			}
		}
	}

	public static boolean moveUp(byte[][] board)
	{
		boolean success = false;
		byte x;
		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			success |= slideArray(board[x]);
		}
		return success;
	}

	public static boolean moveLeft(byte[][] board)
	{
		boolean success;
		rotateBoard(board);
		success = moveUp(board);
		rotateBoard(board);
		rotateBoard(board);
		rotateBoard(board);
		return success;
	}

	public static boolean moveDown(byte[][] board)
	{
		boolean success;
		rotateBoard(board);
		rotateBoard(board);
		success = moveUp(board);
		rotateBoard(board);
		rotateBoard(board);
		return success;
	}

	public static boolean moveRight(byte[][] board)
	{
		boolean success;
		rotateBoard(board);
		rotateBoard(board);
		rotateBoard(board);
		success = moveUp(board);
		rotateBoard(board);
		return success;
	}

	public static boolean findPairDown(byte[][] board)
	{
		boolean success = false;
		byte x;
		byte y;
		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			for (y = 0;y < DefineConstants.SIZE-1;y++)
			{
				if (board[x][y] == board[x][y + 1])
				{
					return true;
				}
			}
		}
		return success;
	}

	public static byte countEmpty(byte[][] board)
	{
		byte x;
		byte y;
		byte count = 0;
		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			for (y = 0;y < DefineConstants.SIZE;y++)
			{
				if (board[x][y] == 0)
				{
					count++;
				}
			}
		}
		return count;
	}

	public static boolean gameEnded(byte[][] board)
	{
		boolean ended = true;
		if (countEmpty(board) > 0)
		{
			return false;
		}
		if (findPairDown(board))
		{
			return false;
		}
		rotateBoard(board);
		if (findPairDown(board))
		{
			ended = false;
		}
		rotateBoard(board);
		rotateBoard(board);
		rotateBoard(board);
		return ended;
	}
//C++ TO JAVA CONVERTER NOTE: This was formerly a static local variable declaration (not allowed in Java):
private static boolean addRandom_initialized = false;

	public static void addRandom(byte[][] board)
	{
	//C++ TO JAVA CONVERTER NOTE: This static local variable declaration (not allowed in Java) has been moved just prior to the method:
	//	static boolean initialized = false;
		byte x;
		byte y;
		byte r;
		byte len = 0;
		byte n;
		byte[][] list = new byte[DefineConstants.SIZE * DefineConstants.SIZE][2];

		if (!addRandom_initialized)
		{
			tangible.RandomNumbers.seed(time(null));
			addRandom_initialized = true;
		}

		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			for (y = 0;y < DefineConstants.SIZE;y++)
			{
				if (board[x][y] == 0)
				{
					list[len][0] = x;
					list[len][1] = y;
					len++;
				}
			}
		}

		if (len > 0)
		{
			r = tangible.RandomNumbers.nextNumber() % len;
			x = list[r][0];
			y = list[r][1];
			n = (tangible.RandomNumbers.nextNumber() % 10) / 9 + 1;
			board[x][y] = n;
		}
	}

	public static void initBoard(byte[][] board)
	{
		byte x;
		byte y;
		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			for (y = 0;y < DefineConstants.SIZE;y++)
			{
				board[x][y] = 0;
			}
		}
		addRandom(board);
		addRandom(board);
		drawBoard(board);
		score = 0;
	}
//C++ TO JAVA CONVERTER NOTE: This was formerly a static local variable declaration (not allowed in Java):
private static boolean setBufferedInput_enabled = true;
//C++ TO JAVA CONVERTER NOTE: This was formerly a static local variable declaration (not allowed in Java):
private static setBufferedInput_termios old = new setBufferedInput_termios();

	public static void setBufferedInput(boolean enable)
	{
	//C++ TO JAVA CONVERTER NOTE: This static local variable declaration (not allowed in Java) has been moved just prior to the method:
	//	static boolean enabled = true;
	//C++ TO JAVA CONVERTER NOTE: This static local variable declaration (not allowed in Java) has been moved just prior to the method:
	//	static struct termios old;
		setBufferedInput_termios new = new setBufferedInput_termios();

		if (enable && !setBufferedInput_enabled)
		{
			// restore the former settings
			tcsetattr(STDIN_FILENO, TCSANOW, old);
			// set the new state
			setBufferedInput_enabled = true;
		}
		else if (!enable && setBufferedInput_enabled)
		{
			// get the terminal settings for standard input
			tcgetattr(STDIN_FILENO, new);
			// we want to keep the old setting to restore them at the end
			old = new;
			// disable canonical mode (buffered i/o) and local echo
			new.c_lflag &= (~ICANON & ~ECHO);
			// set the new settings immediately
			tcsetattr(STDIN_FILENO, TCSANOW, new);
			// set the new state
			setBufferedInput_enabled = false;
		}
	}

	public static int test()
	{
		byte[] array = new byte[DefineConstants.SIZE];
		// these are exponents with base 2 (1=2 2=4 3=8)
		byte[] data = {0,0,0,1, 1,0,0,0, 0,0,1,1, 2,0,0,0, 0,1,0,1, 2,0,0,0, 1,0,0,1, 2,0,0,0, 1,0,1,0, 2,0,0,0, 1,1,1,0, 2,1,0,0, 1,0,1,1, 2,1,0,0, 1,1,0,1, 2,1,0,0, 1,1,1,1, 2,2,0,0, 2,2,1,1, 3,2,0,0, 1,1,2,2, 2,3,0,0, 3,0,1,1, 3,2,0,0, 2,0,1,1, 2,2,0,0};
		byte[] in;
		byte[] out;
		byte t;
		byte tests;
		byte i;
		boolean success = true;

//C++ TO JAVA CONVERTER WARNING: This 'sizeof' ratio was replaced with a direct reference to the array length:
//ORIGINAL LINE: tests = (sizeof(data)/sizeof(data[0]))/(2 *DefineConstants.SIZE);
		tests = (data.length) / (2 * DefineConstants.SIZE);
		for (t = 0;t < tests;t++)
		{
			in = data + t * 2 * DefineConstants.SIZE;
			out = in + DefineConstants.SIZE;
			for (i = 0;i < DefineConstants.SIZE;i++)
			{
				array[i] = in[i];
			}
			slideArray(array);
			for (i = 0;i < DefineConstants.SIZE;i++)
			{
				if (array[i] != out[i])
				{
					success = false;
				}
			}
			if (success == false)
			{
				for (i = 0;i < DefineConstants.SIZE;i++)
				{
					System.out.printf("%d ",in[i]);
				}
				System.out.print("=> ");
				for (i = 0;i < DefineConstants.SIZE;i++)
				{
					System.out.printf("%d ",array[i]);
				}
				System.out.print("expected ");
				for (i = 0;i < DefineConstants.SIZE;i++)
				{
					System.out.printf("%d ",in[i]);
				}
				System.out.print("=> ");
				for (i = 0;i < DefineConstants.SIZE;i++)
				{
					System.out.printf("%d ",out[i]);
				}
				System.out.print("\n");
				break;
			}
		}
		if (success)
		{
			System.out.printf("All %u tests executed successfully\n",tests);
		}
		return !success;
	}

	public static void signal_callback_handler(int signum)
	{
		System.out.print("         TERMINATED         \n");
		setBufferedInput(true);
		System.out.print("\033[?25h\033[m");
		System.exit(signum);
	}

	public static int Main(int argc, String[] args)
	{
		byte[][] board = new byte[DefineConstants.SIZE][DefineConstants.SIZE];
		byte c;
		boolean success;

		if (argc == 2 && strcmp(args[1],"test") == 0)
		{
			return test();
		}
		if (argc == 2 && strcmp(args[1],"blackwhite") == 0)
		{
			scheme = 1;
		}
		if (argc == 2 && strcmp(args[1],"bluered") == 0)
		{
			scheme = 2;
		}

		System.out.print("\033[?25l\033[2J");

		// register signal handler for when ctrl-c is pressed
		signal(SIGINT, signal_callback_handler);

		initBoard(board);
		setBufferedInput(false);
		while (true)
		{
			c = System.in.read();
			switch (c)
			{
				case 97: // 'a' key
				case 104: // 'h' key
				case 68: // left arrow
					success = moveLeft(board);
					break;
				case 100: // 'd' key
				case 108: // 'l' key
				case 67: // right arrow
					success = moveRight(board);
					break;
				case 119: // 'w' key
				case 107: // 'k' key
				case 65: // up arrow
					success = moveUp(board);
					break;
				case 115: // 's' key
				case 106: // 'j' key
				case 66: // down arrow
					success = moveDown(board);
					break;
				default:
					success = false;
			}
			if (success)
			{
				drawBoard(board);
				usleep(150000);
				addRandom(board);
				drawBoard(board);
				if (gameEnded(board))
				{
					System.out.print("         GAME OVER          \n");
					break;
				}
			}
			if (c == 'q')
			{
				System.out.print("        QUIT? (y/n)         \n");
				c = System.in.read();
				if (c == 'y')
				{
					break;
				}
				drawBoard(board);
			}
			if (c == 'r')
			{
				System.out.print("       RESTART? (y/n)       \n");
				c = System.in.read();
				if (c == 'y')
				{
					initBoard(board);
				}
				drawBoard(board);
			}
		}
		setBufferedInput(true);

		System.out.print("\033[?25h\033[m");

		return EXIT_SUCCESS;
	}
}