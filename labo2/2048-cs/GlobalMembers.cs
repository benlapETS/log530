﻿public static class GlobalMembers
{
	/*
	 ============================================================================
	 Name        : 2048.c
	 Author      : Maurits van der Schee
	 Description : Console version of the game "2048" for GNU/Linux
	 ============================================================================
	 */


	public static uint score = 0;
	public static byte scheme = 0;

	public static void getColor(byte value, ref string color, uint length)
	{
		byte[] original = {8,255,1,255,2,255,3,255,4,255,5,255,6,255,7,255,9,0,10,0,11,0,12,0,13,0,14,0,255,0,255,0};
		byte[] blackwhite = {232,255,234,255,236,255,238,255,240,255,242,255,244,255,246,0,248,0,249,0,250,0,251,0,252,0,253,0,254,0,255,0};
		byte[] bluered = {235,255,63,255,57,255,93,255,129,255,165,255,201,255,200,255,199,255,198,255,197,255,196,255,196,255,196,255,196,255,196,255};
		byte[][] schemes = {original,blackwhite,bluered};
//C++ TO C# CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
		byte * background = schemes[scheme] + 0;
//C++ TO C# CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
		byte * foreground = schemes[scheme] + 1;
		if (value > 0)
		{
			while (value-- != 0)
			{
			if (background + 2 < schemes[scheme] + sizeof(byte))
			{
				background += 2;
				foreground += 2;
			}
			}
		}
		snprintf(color,length,"\x001B[38;5;%d;48;5;%dm",*foreground,*background);
	}

	public static void drawBoard(byte[,] board)
	{
		byte x;
		byte y;
		sbyte c;
		string color = new string(new char[40]);
		string reset = "\x001B[m";
		Console.Write("\x001B[H");

		Console.Write("2048.c {0,17:D} pts\n\n",score);

		for (y = 0;y < DefineConstants.SIZE;y++)
		{
			for (x = 0;x < DefineConstants.SIZE;x++)
			{
				getColor(board[x, y], ref color, 40);
				Console.Write("{0}",color);
				Console.Write("       ");
				Console.Write("{0}",reset);
			}
			Console.Write("\n");
			for (x = 0;x < DefineConstants.SIZE;x++)
			{
				getColor(board[x, y], ref color, 40);
				Console.Write("{0}",color);
				if (board[x, y] != 0)
				{
					string s = new string(new char[8]);
					snprintf(s,8,"%u",(uint)1 << board[x, y]);
					byte t = 7 - s.Length;
//C++ TO C# CONVERTER TODO TASK: The following line has a C format specifier which cannot be directly translated to C#:
//ORIGINAL LINE: printf("%*s%s%*s",t-t/2,"",s,t/2,"");
					Console.Write("%*s{1}%*s",t - t / 2,"",s,t / 2,"");
				}
				else
				{
					Console.Write("   ·   ");
				}
				Console.Write("{0}",reset);
			}
			Console.Write("\n");
			for (x = 0;x < DefineConstants.SIZE;x++)
			{
				getColor(board[x, y], ref color, 40);
				Console.Write("{0}",color);
				Console.Write("       ");
				Console.Write("{0}",reset);
			}
			Console.Write("\n");
		}
		Console.Write("\n");
		Console.Write("        ←,↑,→,↓ or q        \n");
		Console.Write("\x001B[A"); // one line up
	}

	public static byte findTarget(byte[] array, byte x, byte stop)
	{
		byte t;
		// if the position is already on the first, don't evaluate
		if (x == 0)
		{
			return x;
		}
		for (t = x - 1;t >= 0;t--)
		{
			if (array[t] != 0)
			{
				if (array[t] != array[x])
				{
					// merge is not possible, take next position
					return t + 1;
				}
				return t;
			}
			else
			{
				// we should not slide further, return this one
				if (t == stop)
				{
					return t;
				}
			}
		}
		// we did not find a
		return x;
	}

	public static bool slideArray(byte[] array)
	{
		bool success = false;
		byte x;
		byte t;
		byte stop = 0;

		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			if (array[x] != 0)
			{
				t = findTarget(array, x, stop);
				// if target is not original position, then move or merge
				if (t != x)
				{
					// if target is zero, this is a move
					if (array[t] == 0)
					{
						array[t] = array[x];
					}
					else if (array[t] == array[x])
					{
						// merge (increase power of two)
						array[t]++;
						// increase score
						score += (uint)1 << array[t];
						// set stop to avoid double merge
						stop = t + 1;
					}
					array[x] = 0;
					success = true;
				}
			}
		}
		return success;
	}

	public static void rotateBoard(byte[,] board)
	{
		byte i;
		byte j;
		byte n = DefineConstants.SIZE;
		byte tmp;
		for (i = 0; i < n / 2; i++)
		{
			for (j = i; j < n - i - 1; j++)
			{
				tmp = board[i, j];
				board[i, j] = board[j, n - i - 1];
				board[j, n - i - 1] = board[n - i - 1, n - j - 1];
				board[n - i - 1, n - j - 1] = board[n - j - 1, i];
				board[n - j - 1, i] = tmp;
			}
		}
	}

	public static bool moveUp(byte[,] board)
	{
		bool success = false;
		byte x;
		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			success |= slideArray(board[x]);
		}
		return success;
	}

	public static bool moveLeft(byte[,] board)
	{
		bool success;
		rotateBoard(board);
		success = moveUp(board);
		rotateBoard(board);
		rotateBoard(board);
		rotateBoard(board);
		return success;
	}

	public static bool moveDown(byte[,] board)
	{
		bool success;
		rotateBoard(board);
		rotateBoard(board);
		success = moveUp(board);
		rotateBoard(board);
		rotateBoard(board);
		return success;
	}

	public static bool moveRight(byte[,] board)
	{
		bool success;
		rotateBoard(board);
		rotateBoard(board);
		rotateBoard(board);
		success = moveUp(board);
		rotateBoard(board);
		return success;
	}

	public static bool findPairDown(byte[,] board)
	{
		bool success = false;
		byte x;
		byte y;
		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			for (y = 0;y < DefineConstants.SIZE-1;y++)
			{
				if (board[x, y] == board[x, y + 1])
				{
					return true;
				}
			}
		}
		return success;
	}

	public static byte countEmpty(byte[,] board)
	{
		byte x;
		byte y;
		byte count = 0;
		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			for (y = 0;y < DefineConstants.SIZE;y++)
			{
				if (board[x, y] == 0)
				{
					count++;
				}
			}
		}
		return count;
	}

	public static bool gameEnded(byte[,] board)
	{
		bool ended = true;
		if (countEmpty(board) > 0)
		{
			return false;
		}
		if (findPairDown(board))
		{
			return false;
		}
		rotateBoard(board);
		if (findPairDown(board))
		{
			ended = false;
		}
		rotateBoard(board);
		rotateBoard(board);
		rotateBoard(board);
		return ended;
	}
//C++ TO C# CONVERTER NOTE: This was formerly a static local variable declaration (not allowed in C#):
private static bool addRandom_initialized = false;

	public static void addRandom(byte[,] board)
	{
	//C++ TO C# CONVERTER NOTE: This static local variable declaration (not allowed in C#) has been moved just prior to the method:
	//	static bool initialized = false;
		byte x;
		byte y;
		byte r;
		byte len = 0;
		byte n;
		byte[,] list = new byte[DefineConstants.SIZE * DefineConstants.SIZE, 2];

		if (!addRandom_initialized)
		{
			RandomNumbers.Seed(time(null));
			addRandom_initialized = true;
		}

		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			for (y = 0;y < DefineConstants.SIZE;y++)
			{
				if (board[x, y] == 0)
				{
					list[len, 0] = x;
					list[len, 1] = y;
					len++;
				}
			}
		}

		if (len > 0)
		{
			r = RandomNumbers.NextNumber() % len;
			x = list[r, 0];
			y = list[r, 1];
			n = (RandomNumbers.NextNumber() % 10) / 9 + 1;
			board[x, y] = n;
		}
	}

	public static void initBoard(byte[,] board)
	{
		byte x;
		byte y;
		for (x = 0;x < DefineConstants.SIZE;x++)
		{
			for (y = 0;y < DefineConstants.SIZE;y++)
			{
				board[x, y] = 0;
			}
		}
		addRandom(board);
		addRandom(board);
		drawBoard(board);
		score = 0;
	}
//C++ TO C# CONVERTER NOTE: This was formerly a static local variable declaration (not allowed in C#):
private static bool setBufferedInput_enabled = true;
//C++ TO C# CONVERTER NOTE: This was formerly a static local variable declaration (not allowed in C#):
private static setBufferedInput_termios old = new setBufferedInput_termios();

	public static void setBufferedInput(bool enable)
	{
	//C++ TO C# CONVERTER NOTE: This static local variable declaration (not allowed in C#) has been moved just prior to the method:
	//	static bool enabled = true;
	//C++ TO C# CONVERTER NOTE: This static local variable declaration (not allowed in C#) has been moved just prior to the method:
	//	static struct termios old;
		setBufferedInput_termios new = new setBufferedInput_termios();

		if (enable && !setBufferedInput_enabled)
		{
			// restore the former settings
			tcsetattr(STDIN_FILENO, TCSANOW, old);
			// set the new state
			setBufferedInput_enabled = true;
		}
		else if (!enable && setBufferedInput_enabled)
		{
			// get the terminal settings for standard input
			tcgetattr(STDIN_FILENO, new);
			// we want to keep the old setting to restore them at the end
			old = new;
			// disable canonical mode (buffered i/o) and local echo
			new.c_lflag &= (~ICANON & ~ECHO);
			// set the new settings immediately
			tcsetattr(STDIN_FILENO, TCSANOW, new);
			// set the new state
			setBufferedInput_enabled = false;
		}
	}

	public static int test()
	{
		byte[] array = new byte[DefineConstants.SIZE];
		// these are exponents with base 2 (1=2 2=4 3=8)
		byte[] data = {0,0,0,1, 1,0,0,0, 0,0,1,1, 2,0,0,0, 0,1,0,1, 2,0,0,0, 1,0,0,1, 2,0,0,0, 1,0,1,0, 2,0,0,0, 1,1,1,0, 2,1,0,0, 1,0,1,1, 2,1,0,0, 1,1,0,1, 2,1,0,0, 1,1,1,1, 2,2,0,0, 2,2,1,1, 3,2,0,0, 1,1,2,2, 2,3,0,0, 3,0,1,1, 3,2,0,0, 2,0,1,1, 2,2,0,0};
		byte[] in;
		byte[] @out;
		byte t;
		byte tests;
		byte i;
		bool success = true;

//C++ TO C# CONVERTER WARNING: This 'sizeof' ratio was replaced with a direct reference to the array length:
//ORIGINAL LINE: tests = (sizeof(data)/sizeof(data[0]))/(2 *DefineConstants.SIZE);
		tests = (data.Length) / (2 * DefineConstants.SIZE);
		for (t = 0;t < tests;t++)
		{
			in = data + t * 2 * DefineConstants.SIZE;
			@out = in + DefineConstants.SIZE;
			for (i = 0;i < DefineConstants.SIZE;i++)
			{
				array[i] = in[i];
			}
			slideArray(array);
			for (i = 0;i < DefineConstants.SIZE;i++)
			{
				if (array[i] != @out[i])
				{
					success = false;
				}
			}
			if (success == false)
			{
				for (i = 0;i < DefineConstants.SIZE;i++)
				{
					Console.Write("{0:D} ",in[i]);
				}
				Console.Write("=> ");
				for (i = 0;i < DefineConstants.SIZE;i++)
				{
					Console.Write("{0:D} ",array[i]);
				}
				Console.Write("expected ");
				for (i = 0;i < DefineConstants.SIZE;i++)
				{
					Console.Write("{0:D} ",in[i]);
				}
				Console.Write("=> ");
				for (i = 0;i < DefineConstants.SIZE;i++)
				{
					Console.Write("{0:D} ",@out[i]);
				}
				Console.Write("\n");
				break;
			}
		}
		if (success)
		{
			Console.Write("All {0:D} tests executed successfully\n",tests);
		}
		return !success;
	}

	public static void signal_callback_handler(int signum)
	{
		Console.Write("         TERMINATED         \n");
		setBufferedInput(true);
		Console.Write("\x001B[?25h\x001B[m");
		Environment.Exit(signum);
	}

	static int Main(int argc, string[] args)
	{
		byte[,] board = new byte[DefineConstants.SIZE, DefineConstants.SIZE];
		sbyte c;
		bool success;

		if (argc == 2 && string.Compare(args[1],"test") == 0)
		{
			return test();
		}
		if (argc == 2 && string.Compare(args[1],"blackwhite") == 0)
		{
			scheme = 1;
		}
		if (argc == 2 && string.Compare(args[1],"bluered") == 0)
		{
			scheme = 2;
		}

		Console.Write("\x001B[?25l\x001B[2J");

		// register signal handler for when ctrl-c is pressed
		signal(SIGINT, signal_callback_handler);

		initBoard(board);
		setBufferedInput(false);
		while (true)
		{
			c = Console.Read();
			switch (c)
			{
				case 97: // 'a' key
				case 104: // 'h' key
				case 68: // left arrow
					success = moveLeft(board);
					break;
				case 100: // 'd' key
				case 108: // 'l' key
				case 67: // right arrow
					success = moveRight(board);
					break;
				case 119: // 'w' key
				case 107: // 'k' key
				case 65: // up arrow
					success = moveUp(board);
					break;
				case 115: // 's' key
				case 106: // 'j' key
				case 66: // down arrow
					success = moveDown(board);
					break;
				default:
					success = false;
				break;
			}
			if (success)
			{
				drawBoard(board);
				usleep(150000);
				addRandom(board);
				drawBoard(board);
				if (gameEnded(board))
				{
					Console.Write("         GAME OVER          \n");
					break;
				}
			}
			if (c == (sbyte)'q')
			{
				Console.Write("        QUIT? (y/n)         \n");
				c = Console.Read();
				if (c == (sbyte)'y')
				{
					break;
				}
				drawBoard(board);
			}
			if (c == (sbyte)'r')
			{
				Console.Write("       RESTART? (y/n)       \n");
				c = Console.Read();
				if (c == (sbyte)'y')
				{
					initBoard(board);
				}
				drawBoard(board);
			}
		}
		setBufferedInput(true);

		Console.Write("\x001B[?25h\x001B[m");

		return EXIT_SUCCESS;
	}

}