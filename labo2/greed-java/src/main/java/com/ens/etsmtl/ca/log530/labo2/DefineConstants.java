package com.ens.etsmtl.ca.log530.labo2;

/**
 * declaration of final and constant variable
 *
 *
 */

final class DefineConstants {
	public static final int HEIGHT = 22;
	public static final int WIDTH = 79;
	public static final String ME = "@";
	public static final int MAXSCORES = 10;
	public static final String LOCKPATH = "/tmp/Greed.lock"; // lock path for
																// high score
																// file
	public static final String LOCALSCOREFILE = ".greedscores";
	public static final int USERNAMELEN = 32; // length of leading segment that
												// we keep
}