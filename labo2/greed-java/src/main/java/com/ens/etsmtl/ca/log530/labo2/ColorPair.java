package com.ens.etsmtl.ca.log530.labo2;

import java.util.HashSet;
import java.util.Set;

import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TextColor;

/**
 * Represents a foreground-background color pair and its optional attribute
 *
 * @author Gabriel Dupont-Francoeur
 *
 */
public class ColorPair {

	private TextColor foregroundColor;
	private TextColor backgroundColor;
	private Set<SGR> options;

	public ColorPair(TextColor foregroundColor, TextColor backgroundColor, SGR option) {
		super();
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
		this.options = new HashSet<>();
		addOption(option);
	}

	public ColorPair(TextColor foregroundColor, TextColor backgroundColor) {
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
		this.options = new HashSet<>();
	}

	public TextColor getForegroundColor() {
		return this.foregroundColor;
	}

	public void setForegroundColor(TextColor foregroundColor) {
		this.foregroundColor = foregroundColor;
	}

	public TextColor getBackgroundColor() {
		return this.backgroundColor;
	}

	public void setBackgroundColor(TextColor backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public Set<SGR> getOptions() {
		return this.options;
	}

	public void addOption(SGR option) {
		this.options.add(option);
	}

	public void removeOption(SGR option) {
		this.options.remove(option);
	}
}
