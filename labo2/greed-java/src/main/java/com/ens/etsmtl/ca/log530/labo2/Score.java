package com.ens.etsmtl.ca.log530.labo2;

import java.io.Serializable;
import java.time.LocalTime;

/*
 * The scorefile is fixed-length binary and consists of
 * structure images - very un-Unixy design!
 */
//C++ TO JAVA CONVERTER NOTE: The following #define macro was replaced in-line:
//ORIGINAL LINE: #define SCOREFILESIZE (MAXSCORES * sizeof(struct score))

/* rnd() returns a random number between 1 and x */
//C++ TO JAVA CONVERTER NOTE: The following #define macro was replaced in-line:
//ORIGINAL LINE: #define rnd(x) (int) ((lrand48() % (x))+1)

/*
 * changing stuff in this struct
 * makes old score files incompatible
 */

public class Score implements Serializable, Comparable<Score> {

	private static final long serialVersionUID = -6829099536689038154L;
	public String user = new String(new char[DefineConstants.USERNAMELEN + 1]);
	public LocalTime time = LocalTime.now();
	public int score;

	/**
	 * get the score of a user
	 * 
	 *
	 */
	public Score(String user, LocalTime time, int score) {
		this.user = user;
		this.time = time;
		this.score = score;
	}

	/**
	 * override of the compareTo metode in other to compare two scores
	 * 
	 * @return the difference between the current score and a choosen score
	 */

	@Override
	public int compareTo(Score o) {

		return o.score - this.score;
	}
}