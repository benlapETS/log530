package com.ens.etsmtl.ca.log530.labo2;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.mortbay.setuid.Passwd;

import com.ens.etsmtl.ca.log530.labo2.tangible.StringFunctions;
import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.ansi.UnixLikeTerminal;
import com.googlecode.lanterna.terminal.ansi.UnixTerminal;

/**
 * greed.c - Written by Matthew T. Day (mday@iconsys.uu.net), 09/06/89
 *
 * Now maintained by Eric S. Raymond <esr@snark.thyrsus.com>. Matt Day
 * dropped out of sight and hasn't posted a new version or patch in many
 * years.
 *
 * 11/15/95 Fred C. Smith (Yes, the SAME Fred C. Smith who did the MS-DOS
 * version), fix the 'p' option so when it removes the highlight from unused
 * possible moves, it restores the previous color. -Some minor changes in
 * the way it behaves at the end of a game, because I didn't like the way
 * someone had changed it to work since I saw it a few years ago (personal
 * preference). -Some style changes in the code, again personal preference.
 * fredex@fcshome.stoneham.ma.us
 */

/**
 * If an environment variable GREEDOPTS is to exist, it will be used to override
 * the default color set, which will be parsed as a string of the form:
 *
 * <c1><c2><c3><c4><c5><c6><c7><c8><c9>[:[p]]
 *
 * where <cn> is a character decribing the color for digit n. The color letters
 * are read as follows: b = blue, g = green, c = cyan, r = red, m = magenta, y =
 * yellow, w = white. In addition, capitalizing a letter turns on the A_BOLD
 * attribute for that letter.
 *
 * If the string ends with a trailing :, letters following are taken as game
 * options. At present, only 'p' (equivalent to an initial 'p' command) is
 * defined.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
public class GlobalMembers {

	private static final String SCOREFILE = null;

	public static String version = "Greed v RELEASE";

	public static int[][] grid = new int[DefineConstants.HEIGHT][DefineConstants.WIDTH];
	public static int y;
	public static int x;
	public static boolean allmoves = false;
	public static boolean havebotmsg = false;
	public static int score = 0;
	public static String cmdname = "greed.java";
	public static Window helpwin = null;
	public static int BUFSIZ = 8192;
	public static UnixLikeTerminal terminal;
	public static Screen screen;
	public static WindowBasedTextGUI gui;
	// keep track of the bottom score text to be able to remove it
	public static TextGraphics botLabel = null;
	public static String botString = null;
	public static Map<Integer, ColorPair> colors = new HashMap<>();

	public static SGR standout = SGR.REVERSE;
	private static String Main_cnames = " rgybmcwRGYBMCW";

	/**
	 * topscores() processes its argument with the high score file, makes any
	 * updates to the file, and outputs the list to the screen.
	 *
	 * @param newscore
	 *            the new score
	 */
	public static void topScores(int newscore) {
		int maxLength;
		String userName;
		Passwd whoami = new Passwd();
		whoami.setPwName(System.getProperty("user.name"));
		Score theScore;
		List<Score> topList = readScores();

		userName = whoami.getPwName();

		maxLength = (userName.length() < DefineConstants.USERNAMELEN) ? userName.length() : DefineConstants.USERNAMELEN;

		theScore = new Score(userName.substring(0, maxLength), LocalTime.now(), newscore);

		topList.add(theScore);

		Collections.sort(topList);

		topList = topList.size() > DefineConstants.MAXSCORES ? topList.subList(0, DefineConstants.MAXSCORES) : topList;

		writeScores(topList);

		printScores(topList, theScore);
	}

	/**
	 * Prints scores to standard out.
	 *
	 * @param topList
	 *            the list to print
	 * @param highlighted
	 *            the score to highlight
	 */
	public static void printScores(List<Score> topList, Score highlighted) {
		int count = 0;
		String ansiPrefix = "", ansiSuffix = "";

		if (topList.size() != 0 && topList.get(0).score != 0) {
			/*
			 * perhaps "greed -s" was run before any greed had been played?
			 */

			/* print out list to screen, highlighting new score, if any */
			for (Score aScore : topList) {
				// TODO : format time properly and bold new score
				// strftime(timestr, (Byte.SIZE / Byte.SIZE),
				// "%Y-%m-%dT%H:%M:%S",
				// when);
				if (highlighted != null && aScore.equals(highlighted)) {
					ansiPrefix = "\033[1m";
					ansiSuffix = "\033[0m";
				} else {
					ansiPrefix = "";
					ansiSuffix = "";
				}
				System.out.printf(ansiPrefix + "%-5d %s %6d %5.2f%% %s\n" + ansiSuffix, count, aScore.time,
						aScore.score, (float) (aScore.score * 100) / (DefineConstants.HEIGHT * DefineConstants.WIDTH),
						aScore.user);

				count++;
			}
		} else {
			System.out.println("No high scores.");
		}
	}

	/**
	 * Reads the score file and returns the contents.
	 *
	 * @return the list of scores contained in the score file
	 */
	public static List<Score> readScores() {
		List<Score> topList = new ArrayList<>();

		String fileName = StringUtils.isNotBlank(SCOREFILE) ? SCOREFILE : DefineConstants.LOCALSCOREFILE;

		try (FileInputStream inputFileStream = new FileInputStream(fileName);
				ObjectInputStream objectInputStream = new ObjectInputStream(inputFileStream)) {
			topList = (ArrayList<Score>) objectInputStream.readObject();
			objectInputStream.close();
			inputFileStream.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (FileNotFoundException fnf) {
			topList = new ArrayList<>();
		} catch (EOFException eof) {
			// file was empty
			topList = new ArrayList<>();
		} catch (IOException i) {
			// there was a problem
			System.out.println(String.format("%s: ~/%s: Cannot open.\n", cmdname, DefineConstants.LOCALSCOREFILE));
			i.printStackTrace();
			System.exit(1);
		}
		return topList;
	}

	/**
	 * Overwrites the score file with the provided list.
	 *
	 * @param topList
	 *            the list of scores to write
	 */
	public static void writeScores(List<Score> topList) {
		File aFile = new File(DefineConstants.LOCALSCOREFILE);
		try {
			aFile.createNewFile();
		} catch (IOException e) {

			e.printStackTrace();
			System.exit(1);
		}
		try (FileOutputStream outputFileStream = new FileOutputStream(aFile);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputFileStream)) {
			objectOutputStream.writeObject(topList);
		} catch (IOException e) {

			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * tunnel() does the main game work. Returns 1 if everything's okay, 0 if
	 * user "died", and -1 if user specified and confirmed 'q' (fast quit)
	 *
	 * @param cmd
	 *            the keystroke to handle
	 * @param attribs
	 *            the attributes for the grid values
	 * @return
	 */
	public static int tunnel(KeyStroke cmd, ColorPair[] attribs) {
		int dy = 0;
		int dx = 0;
		int distance;
		Character code = cmd.getCharacter();
		if (code != null) {
			switch (code) {
			case 'c':
				if (cmd.isCtrlDown()) {
					quit();
				}
				break;
			case 'h':
			case 'H':
			case '4':
				dy = 0;
				dx = -1;
				break;
			case 'j':
			case 'J':
			case '2':
				dy = 1;
				dx = 0;
				break;
			case 'k':
			case 'K':
			case '8':
				dy = -1;
				dx = 0;
				break;
			case 'l':
			case 'L':
			case '6':
				dy = 0;
				dx = 1;
				break;
			case 'b':
			case 'B':
			case '1':
				dy = 1;
				dx = -1;
				break;
			case 'n':
			case 'N':
			case '3':
				dy = dx = 1;
				break;
			case 'y':
			case 'Y':
			case '7':
				dy = dx = -1;
				break;
			case 'u':
			case 'U':
			case '9':
				dy = -1;
				dx = 1;
				break;
			case 'p':
			case 'P':
				allmoves = !allmoves;
				showmoves(allmoves, attribs);
				move(y, x);
				refresh();
				return (1);
			case 'q':
			case 'Q':
				quit();
				return (1);
			case '?':
				help();
				return (1);
			case '\14':
			case '\22':
				refresh();
			default:
				return (1);
			}
		} else {
			// since the keytypes aren't characters we have to
			// treat
			// them here
			if (cmd.getKeyType().equals(KeyType.ArrowLeft)) {
				dy = 0;
				dx = -1;
			} else if (cmd.getKeyType().equals(KeyType.ArrowDown)) {
				dy = 1;
				dx = 0;
			} else if (cmd.getKeyType().equals(KeyType.ArrowUp)) {
				dy = -1;
				dx = 0;
			} else if (cmd.getKeyType().equals(KeyType.ArrowRight)) {
				dy = 0;
				dx = 1;
			}
		}
		distance = (y + dy >= 0 && x + dx >= 0 && y + dy < DefineConstants.HEIGHT && x + dx < DefineConstants.WIDTH)
				? grid[y + dy][x + dx] : 0;

		int j = y;
		int i = x;
		int d = distance;

		do {
			j += dy;
			i += dx;
			if (j >= 0 && i >= 0 && j < DefineConstants.HEIGHT && i < DefineConstants.WIDTH && grid[j][i] != 0) {
				continue;
			} else if (othermove(dy, dx) == 0) {
				j -= dy;
				i -= dx;
				mvaddch(y, x, " ");
				while (y != j || x != i) {
					y += dy;
					x += dx;
					score++;
					mvaddch(y, x, " ");
				}
				mvaddch(y, x, "*");
				showscore();
				return (0);
			} else {
				botmsg("Bad move.", true);
				return (1);
			}
		} while (--d != 0);

		/* remove possible moves */
		if (allmoves) {
			showmoves(false, attribs);
		}

		if (havebotmsg) {
			mvaddch(23, 40, String.format("%s - Hit '?' for help.", version));
			havebotmsg = false;
		}

		mvaddch(y, x, " ");
		do {
			y += dy;
			x += dx;
			score++;
			grid[y][x] = 0;
			mvaddch(y, x, " ");
		} while (--distance != 0);
		try {
			terminal.enableSGR(standout);
			mvaddch(y, x, DefineConstants.ME);
			terminal.disableSGR(standout);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (allmoves) {
			showmoves(true, attribs);
		}
		showscore();
		return (1);
	}

	/**
	 * othermove() checks area for an existing possible move. bady and badx are
	 * direction variables that tell othermove() they are already no good, and
	 * to not process them. I don't know if this is efficient, but it works!
	 *
	 * @param bady
	 *            a wrong y-axis move direction
	 * @param badx
	 *            a wrong x-axis move direction
	 * @return 1 if there exists a right move, else 0 if no more move is
	 *         possible
	 */
	public static int othermove(int bady, int badx) {
		int dy = -1;
		int dx;

		for (; dy <= 1; dy++) {
			for (dx = -1; dx <= 1; dx++) {
				if ((dy == 0 && dx == 0) || (dy == bady && dx == badx) || y + dy < 0 || x + dx < 0
						|| y + dy >= DefineConstants.HEIGHT || x + dx >= DefineConstants.WIDTH) {

					continue;
				} else {
					int j = y;
					int i = x;
					int d = grid[y + dy][x + dx];
					if (d == 0) {
						continue;
					}
					do {
						j += dy;
						i += dx;
						if (j < 0 || i < 0 || j >= DefineConstants.HEIGHT || i >= DefineConstants.WIDTH
								|| grid[j][i] == 0) {
							break;
						}
					} while (--d != 0);
					if (d == 0) {
						return 1;
						/*
						 * if "d" got to 0, move was okay
						 *
						 */
					}
				}
			}
		}
		return 0;
	}

	/**
	 * botmsg() writes "msg" at the middle of the bottom line of the screen.
	 * Boolean "backcur" specifies whether to put cursor back on the grid or
	 * leave it on the bottom line (e.g. for questions).
	 *
	 * @param msg
	 *            the message to be written at the bottom line
	 * @param backcur
	 *            whether the cursor should be put back on the grid or not
	 */
	public static void botmsg(String msg, boolean backcur) {
		// TODO : find a cleaner way to erase previous message?
		if (botLabel == null) {
			botLabel = screen.newTextGraphics();
			botLabel.fillRectangle(new TerminalPosition(40, 23), new TerminalSize(DefineConstants.WIDTH - 40, 1), ' ');
			botLabel.putString(40, 23, msg);
		} else {
			botLabel.clearModifiers();
			botLabel.fillRectangle(new TerminalPosition(40, 23), new TerminalSize(DefineConstants.WIDTH - 40, 1), ' ');
			botLabel.putString(40, 23, msg);
		}
		if (backcur) {
			move(y, x);
		}
		refresh();
		havebotmsg = true;
	}

	/**
	 * Refreshes the terminal screen.
	 */
	private static void refresh() {
		try {
			screen.refresh();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Moves the cursor at the specified position.
	 *
	 * @param y
	 * @param x
	 */
	private static void move(int y, int x) {
		screen.setCursorPosition(new TerminalPosition(x, y));
	}

	/**
	 * quit() is run when the user hits ^C or ^\, it queries the user if he
	 * really wanted to quit, and if so, checks the high score stuff (with the
	 * current score) and quits; otherwise, simply returns to the game.
	 */
	// Removed the int arg as it was unused in the original c program
	public static void quit() {
		Character ch;
		KeyStroke ks = null;
		// osigDelegate osig = signal(this.SIGINT, SIG_IGN);
		// signal(this.SIGQUIT, SIG_IGN);

		if (screen != null) {
			botmsg("Really quit? ", false);
			try {
				ks = screen.readInput();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ch = ks.getCharacter();
			if (ch != null && Character.toLowerCase(ch) != 'y') {
				move(y, x);
				// signal(this.SIGINT, osig); // reset old signal
				// signal(this.SIGQUIT, osig);
				refresh();
				return;
			}
			move(23, 0);
			refresh();
			endwin();
			System.out.println("\n");
			topScores(score);
		}
		System.exit(0);
	}

	@FunctionalInterface
	interface osigDelegate {
		void invoke();
	}

	/**
	 * out() is run when the signal SIGTERM is sent, it corrects the terminal
	 * state (if necessary) and exits.
	 */
	// Removed in argument as it was never used in the c program
	public static void out() {
		endwin();
		System.exit(0);
	}

	/**
	 * usage() prints out the proper command line usage for Greed and exits.
	 */
	public static void usage() {
		System.out.println(String.format("Usage: %s [-p] [-s]\n", cmdname));
		System.exit(1);
	}

	/**
	 * showscore() prints the score and the percentage of the screen eaten at
	 * the beginning of the bottom line of the screen, moves the cursor back on
	 * the grid, and refreshes the screen.
	 */
	public static void showscore() {
		TextGraphics botLabel = null;
		botLabel = screen.newTextGraphics();
		botLabel.putString(7, 23, String.format("%d  %.2f%%", score,
				(float) (score * 100) / (DefineConstants.HEIGHT * DefineConstants.WIDTH)));
		// mvprintw(23, 7, );
		move(y, x);
		refresh();
	}

	/**
	 * showmoves() is nearly identical to othermove(), but it highlights
	 * possible moves instead. "on" tells showmoves() whether to add or remove
	 * moves.
	 *
	 * @param on
	 *            whether moves should be shown or not
	 * @param attribs
	 *            the array of color attributes corresponding to the grid values
	 */
	public static void showmoves(boolean on, ColorPair[] attribs) {
		// TODO : do this
		int dy = -1;
		int dx;

		for (; dy <= 1; dy++) {
			if ((y + dy < 0 || y + dy >= DefineConstants.HEIGHT)) {
				continue;
			}
			for (dx = -1; dx <= 1; dx++) {
				if (x + dx < 0 || x + dx >= DefineConstants.WIDTH) {
					continue;
				}
				int j = y;
				int i = x;
				int d = grid[y + dy][x + dx];

				if (d == 0) {
					continue;
				}
				do {
					j += dy;
					i += dx;
					if (j < 0 || i < 0 || j >= DefineConstants.HEIGHT || i >= DefineConstants.WIDTH
							|| grid[j][i] == 0) {
						break;
					}
				} while (--d != 0);
				if (d == 0) {
					int k = y;
					int l = x;
					int d2 = grid[y + dy][x + dx];

					/*
					 * The next section chooses inverse-video * or not, and then
					 * "walks" chosen valid * move, reprinting characters with
					 * new mode
					 */
					try {
						if (on) {
							terminal.enableSGR(standout);
						}
						do {
							k += dy;
							l += dx;

							if (!on && has_colors()) {
								int newval = grid[k][l];
								mvaddch(k, l, Integer.toString(newval), attribs[newval - 1]);
							} else {
								mvaddch(k, l, Integer.toString(grid[k][l]));
							}
						} while (--d2 != 0);
						if (on) {
							terminal.disableSGR(standout);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * This will always return true, it is already abstracted away in Lanterna.
	 *
	 * @return whether the terminal has color support
	 */
	private static boolean has_colors() {
		// TODO : remove this?
		return true;
	}

	/**
	 * Main method with the input loop. This handles most initializations.
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		int val;
		ColorPair[] attribs = new ColorPair[9];
		String colors;
		Random r = new Random();
		PrintStream ps = null;
		try {
			cmdname = DefineConstants.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		} catch (URISyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		// a log file since ouput can't be read since terminal exits
		File file = new File("greedlog.log");
		try {
			ps = new PrintStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// output errors to a simple debug file
		if (ps != null) {
			System.setErr(ps);
		}

		if (args.length == 1) {
			if (args[0].length() != 2 || args[0].charAt(0) != '-') {
				usage();
			}
			if (args[0].charAt(1) == 's') {
				printScores(readScores(), null);
				System.exit(0);
			}
		} else if (args.length > 1) {
			usage();
		}

		try {

			terminal = new UnixTerminal(System.in, System.out, Charset.defaultCharset(),
					UnixLikeTerminal.CtrlCBehaviour.TRAP);
			terminal.setTerminalSize(DefineConstants.WIDTH, DefineConstants.HEIGHT);
			terminal.setTitle("greed.java");

			screen = new TerminalScreen(terminal);
			screen.startScreen();

			gui = new MultiWindowTextGUI(screen);
			// don't think we need this, it should/might be handled by jcurses I
			// think
			// initscr();
			// keypad(stdscr, true);
			// cbreak();
			// noecho();

			// don't need to initialize random
			// srand48(time(0) ^ getpid() << 16);
			/*
			 * * initialize the random seed
			 *
			 * with a unique number
			 */

			if (has_colors()) {
				start_color();
				init_pair(1, TextColor.ANSI.YELLOW, TextColor.ANSI.BLACK);
				init_pair(2, TextColor.ANSI.RED, TextColor.ANSI.BLACK);
				init_pair(3, TextColor.ANSI.GREEN, TextColor.ANSI.BLACK);
				init_pair(4, TextColor.ANSI.CYAN, TextColor.ANSI.BLACK);
				init_pair(5, TextColor.ANSI.MAGENTA, TextColor.ANSI.BLACK);

				attribs[0] = COLOR_PAIR(1);
				attribs[1] = COLOR_PAIR(2);
				attribs[2] = COLOR_PAIR(3);
				attribs[3] = COLOR_PAIR(4);
				attribs[4] = COLOR_PAIR(5);
				attribs[5] = new ColorPair(attribs[0].getForegroundColor(), attribs[0].getBackgroundColor(), SGR.BOLD);
				attribs[6] = new ColorPair(attribs[1].getForegroundColor(), attribs[1].getBackgroundColor(), SGR.BOLD);
				attribs[7] = new ColorPair(attribs[2].getForegroundColor(), attribs[2].getBackgroundColor(), SGR.BOLD);
				attribs[8] = new ColorPair(attribs[3].getForegroundColor(), attribs[3].getBackgroundColor(), SGR.BOLD);

				if ((colors = System.getenv("GREEDOPTS")) != null) {

					char cp;
					int charPos;
					int i;
					for (i = 0; i < colors.length(); i++) {
						cp = colors.charAt(i);
						if (cp == ':') {
							while (i++ < colors.length()) {
								cp = colors.charAt(i);
								if (cp == 'p') {
									allmoves = true;
								}
							}
							break;
						}
						if (StringFunctions.strChr(Main_cnames, cp) != null) {
							charPos = Main_cnames.indexOf(Character.toLowerCase(cp));
							if (!Character.isWhitespace(cp) && charPos > 0) {
								// do -1 because of the leading space
								init_pair(i - 1, TextColor.ANSI.values()[charPos], TextColor.ANSI.BLACK);
								attribs[i] = COLOR_PAIR(i - 1);
								if (Character.isUpperCase(colors.charAt(i))) {
									attribs[i].addOption(SGR.BOLD);
								}
							}
						}
					}
				}
			}

			for (y = 0; y < DefineConstants.HEIGHT; y++) {
				for (x = 0; x < DefineConstants.WIDTH; x++) {
					int newval = Math.abs((new Random().nextInt() % (9))) + 1;
					grid[y][x] = newval;
					if (has_colors()) {
						// apply an attrib using the val we got
						// attron(attribs[newval - 1]);
						mvaddch(y, x, Integer.toString(newval), attribs[newval - 1]);
						// attroff(attribs[newval - 1]);
					} else {

						mvaddch(y, x, Integer.toString(newval));
					}
				}
			}
			TextGraphics scoreLabel = screen.newTextGraphics();
			scoreLabel.setForegroundColor(TextColor.ANSI.WHITE);
			scoreLabel.setBackgroundColor(TextColor.ANSI.BLACK);
			scoreLabel.putString(0, 23, "Score : ");
			TextGraphics helpLabel = screen.newTextGraphics();
			helpLabel.setForegroundColor(TextColor.ANSI.WHITE);
			helpLabel.setBackgroundColor(TextColor.ANSI.BLACK);
			helpLabel.putString(40, 23, String.format("%s - Hit '?' for help.", version));
			// this was mvprintw(23, 40, "%s - Hit '?' for help.", version);
			y = Math.abs((r.nextInt() % (DefineConstants.HEIGHT)));
			x = Math.abs((r.nextInt() % (DefineConstants.WIDTH)));
			// TODO : highlight with ANSI standout
			terminal.enableSGR(standout);
			mvaddch(y, x, DefineConstants.ME);
			terminal.disableSGR(standout);
			grid[y][x] = 0;

			if (allmoves) {
				showmoves(true, attribs);
			}
			showscore();

			/*
			 * main loop, gives tunnel() a user command
			 *
			 */
			// tangible.RefObject<Integer> tempRef_attribs = new
			// tangible.RefObject<Integer>(attribs);

			// flush terminal if not already done
			screen.refresh();

			while ((val = tunnel(screen.readInput(), attribs)) > 0) {
				continue;
			}

			if (val == 0) {
				botmsg("Hit any key..", false);
				screen.readInput();
			}

			move(23, 0);
			refresh();
			endwin();
			System.out.println("\n");
			topScores(score);
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Adds the string at the position using default colors.
	 *
	 * @param y
	 *            the row position on the screen
	 * @param x
	 *            the column position on the screen
	 * @param chars
	 *            the characters to write on the screen
	 */
	private static void mvaddch(int y, int x, String chars) {
		mvaddch(y, x, chars, null);
	}

	/**
	 * Adds the String to the specified position on the screen.
	 *
	 * @param y
	 *            the row position on the screen
	 * @param x
	 *            the column position on the screen
	 * @param chars
	 *            the characters to write on the screen
	 * @param attributes
	 *            the attributes associated to grid values
	 */
	private static void mvaddch(int y, int x, String chars, ColorPair attributes) {
		// TODO : moves cursor to position and places the character, the c
		// original c function advances 1 position so watch out this might cause
		// problems
		SGR[] optionsArray = new SGR[0];
		try {
			TextGraphics graphics = screen.newTextGraphics();

			if (attributes != null) {
				optionsArray = attributes.getOptions().toArray(optionsArray);
				graphics.setForegroundColor(attributes.getForegroundColor());
				graphics.setBackgroundColor(attributes.getBackgroundColor());
				if (optionsArray.length > 0) {
					graphics.enableModifiers(optionsArray);
				}
			}
			graphics.putString(x, y, chars);
			screen.refresh();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Ends the current screen.
	 */
	private static void endwin() {
		try {
			screen.stopScreen();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Returns the color pair for this index.
	 *
	 * @param i
	 *            the color pair index requested
	 * @return the color pair for this index
	 */
	private static ColorPair COLOR_PAIR(int i) {
		return colors.get(new Integer(i));
	}

	/**
	 * Associates the colors to the provided index
	 *
	 * @param i
	 *            the index
	 * @param foreGroundColor
	 *            the text color
	 * @param backGroundColor
	 *            the background color to apply
	 */
	private static void init_pair(int i, TextColor foreGroundColor, TextColor backGroundColor) {
		colors.put(i, new ColorPair(foreGroundColor, backGroundColor));
	}

	/**
	 * start_color does nothing in this java iteration of greed.
	 */
	private static void start_color() {
		// TODO : start colors on the terminal... is that necessary here?

	}

	/**
	 * help() simply creates a new window over stdscr, and writes the help info
	 * inside it. Uses macro msg() to save space.
	 */
	public static void help() {
		// TODO : make help window in jcurses : this means the window in main
		// needs to be at class member level

		// if (helpwin == null) {
		// helpwin = newwin(18, 65, 1, 7);
		// box(helpwin, ACS_VLINE, ACS_HLINE);
		// waddch(helpwin, ACS_ULCORNER);
		// mvwaddch(helpwin, 0, 64, ACS_URCORNER);
		// mvwaddch(helpwin, 17, 0, ACS_LLCORNER);
		// mvwaddch(helpwin, 17, 64, ACS_LRCORNER);
		// box(helpwin, '|', '-');
		// waddch(helpwin, '+');
		// mvwaddch(helpwin, 0, 64, '+');
		// mvwaddch(helpwin, 17, 0, '+');
		// mvwaddch(helpwin, 17, 64, '+');
		// mvwprintw(helpwin, 1, 2, "Welcome to %s, by Matthew Day
		// <mday@iconsys.uu.net>.", version);
		// mvwaddstr(helpwin, 3, 2, msg);
		// mvwaddstr(helpwin, 4, 2, msg);
		// mvwaddstr(helpwin, 5, 2, msg);
		// mvwprintw(helpwin, 6, 2, " 'hjklyubn'. Your location is signified by
		// the '%c' symbol.", DefineConstants.ME);
		// mvwaddstr(helpwin, 7, 2, msg);
		// mvwaddstr(helpwin, 8, 2, msg);
		// mvwaddstr(helpwin, 9, 2, msg);
		// mvwaddstr(helpwin, 10, 2, msg);
		// mvwaddstr(helpwin, 11, 2, msg);
		// mvwaddstr(helpwin, 12, 2, msg);
		// mvwaddstr(helpwin, 13, 2, msg);
		// mvwaddstr(helpwin, 14, 2, msg);
		// mvwaddstr(helpwin, 15, 2, msg);
		// mvwaddstr(helpwin, 16, 2, msg);
		// wmove(helpwin, 17, 64);
		// wrefresh(helpwin);
		// } else {
		// touchwin(helpwin);
		// wrefresh(helpwin);
		// }
		// wgetch(helpwin);
		// touchwin(stdscr);
		// refresh();

	}

}