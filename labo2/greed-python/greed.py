'''
 * greed.c - Written by Matthew T. Day (mday@iconsys.uu.net), 09/06/89
 *
 * Now maintained by Eric S. Raymond <esr@snark.thyrsus.com>.  Matt
 * Day dropped out of sight and hasn't posted a version or patch
 * in many years.
 *
 * 11/15/95 Fred C. Smith (Yes, SAME Fred C. Smith who did the MS-DOS
 *          version), the 'p' option so when it removes the highlight
 *          from unused possible moves, restores the previous color.
 *          -Some minor changes in the way it behaves at the end of a game,
 *          because I didn't like the way someone had changed it to work
 *          since I saw it a few years ago (personal preference).
 *          -Some style changes in the code, personal preference.
 *          fredex@fcshome.stoneham.ma.us
 '''

'''
 * When using a curses library with color capability, will
 * detect color curses(3) if you have it and generate the board in
 * color, color to each of the digit values. This will also enable
 * checking of an environment variable GREEDOPTS to override the
 * default color set, will be parsed as a string of the form:
 *
 *	<c1><c2><c3><c4><c5><c6><c7><c8><c9>[:[p]]
 *
 * where <cn> is a character decribing the color for digit n.
 * The color letters are read as follows:
 b = blue,
 g = green,
 c = cyan,
 r = red,
 m = magenta,
 y = yellow,
 w = white.
 * In addition, a letter turns on the A_BOLD attribute for that
 * letter.
 *
 * If the string ends with a trailing :, following are taken as game
 * options. At present, only 'p' (equivalent to an initial 'p' command) is
 * defined.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 '''
import datetime, sys, curses, random, time, os, re, pwd, stat
from signal import *
from curses import *
from datetime import datetime

version = "Greed vpy"

#ifdef A_COLOR
#include <ctype.h>
#endif

HEIGHT = 22
WIDTH = 79
ME = '@'

'''
 * The scorefile is fixed-length binary and consists of
 * structure images - very un-Unixy design!
 '''
MAXSCORES = 10
SCOREFILESIZE = (MAXSCORES * 1024)

''' rnd() returns a random number between 1 and x '''
#define rnd(x) (int) ((lrand48() % (x))+1)

#lock path for high score file
LOCKPATH = "/tmp/Greed.lock"

#define
LOCALSCOREFILE	= ".greedscores"

#length of leading segment that we keep
USERNAMELEN = 32

# TODO Code smell 6: Move method in data class
class Score:
    '''
    * changing stuff in class
    * makes old score files incompatible
    '''
    def __init__(self, username="", score=0, time=datetime.now()):
        self.username = username
        self.score = score
        self.time = time

# TODO Code smell 1: What to do with global variables?
grid = []
x = 1
y = 1
score = 0

# booleans
allmoves = False
havebotmsg = False

cmdname = ""

# curses stuff
stdscr = None
helpwin = None


def botmsg(msg, backcur):
    '''
    * botmsg() writes "msg" at the middle of the bottom line of the screen.
    * Boolean "backcur" specifies whether to put cursor back on the grid or
    * leave it on the bottom line (e.g. for questions).
    '''
    global havebotmsg
    global stdscr
    stdscr.addstr(23, 40, msg)
    stdscr.clrtoeol()
    if backcur:
        stdscr.move(y, x)
    stdscr.refresh()
    havebotmsg = True


def app_quit(sig, sig2):
    '''
    * quit() is run when the user hits ^C or ^\, queries the user if he
    * really wanted to quit, if so, the high score stuff (with the
    * current score) and quits; otherwise, returns to the game.
    '''
    ''' save old signal '''
    osig = signal(SIGINT, SIG_IGN)
    signal(SIGQUIT, SIG_IGN)
    global stdscr, score

    if stdscr:
        botmsg("Really quit? ", False)
    try:
        ch = sys.stdin.read(1)
    except:
        print("error")

    if ch not in ['y', 'Y', 121]:
        stdscr.move(y, x)
        ''' reset old signal '''
        signal(SIGINT, osig)
        signal(SIGQUIT, osig)
        stdscr.refresh()
        return

    stdscr.move(23, 0)
    stdscr.refresh()
    endwin()
    print("\n")
    topscores(score)
    sys.exit(0)


def out(onsig):
    '''
    * out() is run when the signal SIGTERM is sent, corrects the terminal
    * state (if necessary) and exits.
    '''
    if endwin():
        sys.exit(0)


def usage():
    ''' usage() prints out the proper command line usage for Greed and exits. '''
    print("Usage: {} [-p] [-s]\n", cmdname)
    sys.exit(1)


def showscore():
    '''
    * showscore() prints the score and the percentage of the screen eaten
    * at the beginning of the bottom line of the screen, the
    * cursor back on the grid, refreshes the screen.
    '''
    global stdscr, score
    stdscr.addstr(
            23, 7,
            "{0} {1:.2f}".format(score, (score * 100) / (HEIGHT * WIDTH))
        )
    stdscr.move(y, x)
    stdscr.refresh()

# TODO Code smell 3: Long main method that will have to be break in part
def main():
    global stdscr, score, y, x, grid
    val = 1
    attribs = [0]*9
    colors = None
    allmoves = False
    ''' save the command name '''
    cmdname = sys.argv[0]

    ''' process the command line '''
    if (len(sys.argv) == 2):
        if len(argv[1]) != 2 or sys.argv[1][0] != '-':
            usage()
        if sys.argv[1][1] == 's':
            topscores(0)
            exit(0)
    elif (len(sys.argv) > 2):
        ''' can't have > 2 arguments '''
        usage()

    ''' catch off the signals '''
    signal(SIGINT, app_quit)
    signal(SIGQUIT, app_quit)
    signal(SIGTERM, out)
    stdscr = initscr()
    ''' set up the terminal modes '''
    stdscr.keypad(True)
    cbreak()
    noecho()

    ''' initialize the random seed with a unique number '''
    if has_colors():
        start_color()
        init_pair(1, COLOR_YELLOW, COLOR_BLACK)
        init_pair(2, COLOR_RED, COLOR_BLACK)
        init_pair(3, COLOR_GREEN, COLOR_BLACK)
        init_pair(4, COLOR_CYAN, COLOR_BLACK)
        init_pair(5, COLOR_MAGENTA, COLOR_BLACK)

        attribs[0] = color_pair(1)
        attribs[1] = color_pair(2)
        attribs[2] = color_pair(3)
        attribs[3] = color_pair(4)
        attribs[4] = color_pair(5)
        attribs[5] = color_pair(1) | A_BOLD
        attribs[6] = color_pair(2) | A_BOLD
        attribs[7] = color_pair(3) | A_BOLD
        attribs[8] = color_pair(4) | A_BOLD
        try:
            colors = os.getenv("GREEDOPTS")
        except:
            print("error")

        if colors is not None:
            cnames = " rgybmcwRGYBMCW"

            for cp in colors:
                if cp is ':':
                    break
                elif cnames.find(cp) > -1:
                    if cp is not ' ':
                        init_pair(cp-colors+1,
                            cnames.find(cp.lower())-cnames, COLOR_BLACK)
                        attribs[cp-colors]=color_pair(cp-colors+1)
                        if cp.isupper():
                            attribs[cp-colors] |= A_BOLD

            p = re.compile('(?<=:)p')
            if p.match(colors):
                allmoves = True

    ''' fill the grid array and '''
    for j in range(HEIGHT):
        ''' print numbers out '''
        grid.append([])
        for i in range(WIDTH):
            if has_colors():
                newval = random.randint(1, 9)
                # C curses commands
                stdscr.attron(attribs[newval - 1])
                grid[j].append(newval)
                stdscr.addch(j, i, str(newval))
                stdscr.attroff(attribs[newval - 1])
            else:
                grid[j].append(random.randint(1, 9))
                stdscr.addch(j, i, str(grid[j][i]))


    ''' initialize bottom line '''
    stdscr.addstr(23, 0, "Score: ")
    stdscr.addstr(23, 40, version + " - Hit '?' for help.")
    y = random.randint(1, HEIGHT)-1
    ''' random initial location '''
    x = random.randint(1, WIDTH)-1
    stdscr.attron(A_STANDOUT)
    stdscr.addch(y, x, ME)
    stdscr.attroff(A_STANDOUT)
    ''' eat initial square '''
    grid[y][x] = 0

    if (allmoves):
        showmoves(True, attribs)
    showscore()

    ''' main loop, tunnel() a user command '''
    while val > 0:
        character = stdscr.getkey()
        val = tunnel(character, attribs)
        if (val <= 0):
            break

    if not val:
        ''' if didn't quit by 'q' cmd '''
        ''' then let user examine     '''
        botmsg("Hit any key..", False)
        ''' final screen              '''
        sys.stdin.read(1)


    stdscr.move(23, 0)
    stdscr.refresh()
    endwin()
    ''' writes two newlines '''
    puts("\n")
    topscores(score)
    exit(0)


def tunnel(cmd, attribs):
    '''
     * tunnel() does the main game work.  Returns 1 if everything's okay, 0 if
     * user "died", and -1 if user specified and confirmed 'q' (fast quit).
     '''
    # int dy, dx, distance
    dy = dx = distance = 0
    global y, x, grid
    global allmoves, havebotmsg, stdscr, score

    # TODO Code smell 2: Refactor this huge "switch case"
    if cmd in ['h', 'H', '4', 'KEY_LEFT']:
        dy = 0
        dx = -1
    elif cmd in ['j', 'J', '2', 'KEY_DOWN']:
        dy = 1
        dx = 0
    elif cmd in ['k', 'K', '8', 'KEY_UP']:
        dy = -1
        dx = 0
    elif cmd in ['l', 'L', '6', 'KEY_RIGHT']:
        dy = 0
        dx = 1
    elif cmd in ['b', 'B', '1']:
        dy = 1
        dx = -1
    elif cmd in ['n', 'N', '3']:
        dy = 1
        dx = 1
    elif cmd in ['y', 'Y', '7']:
        dy = -1
        dx = -1
    elif cmd in ['u', 'U', '9']:
        dy = -1
        dx = 1
    elif cmd in ['p', 'P', '112']:
        allmoves = not allmoves
        showmoves(allmoves, attribs)
        stdscr.move(y, x)
        stdscr.refresh()
        return 1
    elif cmd in ['q', 'Q', 113]:
        app_quit(0, 0)
        return 1
    elif cmd in ['?', 63]:
        help()
        return 1
    elif cmd in ['\14', '\22']:	# ^L or ^R (redraw) in original C code, needs to be checked
        stdscr.wrefresh()
    else:
        return 1

    if y+dy >= 0 and x+dx >= 0 and y+dy < HEIGHT and x+dx < WIDTH:
         distance = grid[y+dy][x+dx]
    else:
        distance = 0

    j = y
    i = x
    d = distance

    while d != 0:
        j += dy
        i += dx
        result = navigate_screen(i, j, dx, dy)
        if result in [0, 1]:
            return result
        d-=1
        if (d==0):
            break
    # TODO Code smell 4: Extract method from comment
    ''' remove possible moves '''
    if (allmoves):
        showmoves(False, attribs)

    if (havebotmsg):
        ''' if old bottom msg exists '''
        stdscr.addstr(23, 40, "{} - Hit '?' for help.".format(version))
        havebotmsg = False

    ''' erase old ME '''
    stdscr.addch(y, x, ' ')

    while distance != 0:
        # TODO Code smell 4: Extract method from comment
        ''' print good path '''
        y += dy
        x += dx
        score += 1
        grid[y][x] = 0
        stdscr.addch(y, x, ord(' '))
        distance -= 1
        if distance == 0:
            break

    stdscr.attron(A_STANDOUT)
    ''' put ME '''
    stdscr.addch(y, x, ME)
    stdscr.attroff(A_STANDOUT)
    if (allmoves):
        ''' put possible moves '''
        showmoves(True, attribs)
    ''' does stdscr.refresh() finally '''
    showscore()
    return 1


def othermove(bady, badx):
    '''
     * othermove() checks area for an existing possible move.  bady and
     * badx are direction variables that tell othermove() they are
     * already no good, to not process them.  I don't know if this
     * is efficient, it works!
     '''
    dy = -1
    global grid, x, y, HEIGHT, WIDTH

    while (dy <= 1):
        dx = -1
        while (dx <= 1):
            not_d = not dy and not dx
            bad = dy == bady and dx == badx
            coord_bool = y+dy < 0 and x+dx < 0
            coord_max = y+dy >= HEIGHT and x+dx >= WIDTH
            some_logic = coord_bool and coord_max

            if not_d or bad or some_logic:
                ''' don't do 0, or bad coordinates '''
                continue
            else:
                j=y
                i=x
                d=grid[y+dy][x+dx]

                if (not d):
                    continue
                while True:
                    # TODO Code smell 4: Extract method from comment
                    ''' "walk" the path, checking '''
                    j += dy
                    i += dx
                    if (j < 0 or i < 0 or j >= HEIGHT or
                                            i >= WIDTH or grid[j][i] == 0 or grid[j][i] is None ):
                        break
                    d -=1
                    if d == 0:
                        break

                if d == 0:
                    ''' if "d" got to 0,  move was okay.   '''
                    return True
            dx+=1
        dy+=1
    ''' no good moves were found '''
    return False

def navigate_screen(i, j, dx, dy):
    global stdscr, x, y, grid

    ''' if off the screen '''
    if j >= 0 and i >= 0 and j < HEIGHT and i < WIDTH and grid[j][i] != 0:
        return 2
    elif not othermove(dy, dx):
        ''' no other good move '''
        j -= dy
        i -= dx
        stdscr.addch(y, x, ' ')

        while y != j or x != i:
            ''' so we manually '''
            y += dy
            ''' print chosen path '''
            x += dx
            score += 1
            stdscr.addch(y, x, ' ')

        ''' with a '*' '''
        stdscr.addch(y, x, '*')
        ''' print final score '''
        showscore()
        return 0
    else:
        ''' otherwise prevent bad move '''
        botmsg("Bad move.", True)
        return 1

# TODO Code smell 5: Extract method from long method
def showmoves(on, attribs):
    '''
     * showmoves() is nearly identical to othermove(), it highlights possible
     * moves instead.  "on" tells showmoves() whether to add or remove moves.
     '''
    global x, y, grid
    dy = -1

    while (dy <= 1):
        if (y+dy < 0 or y+dy >= HEIGHT):
            dy += 1
            continue
        dx = -1
        while (dx <= 1):
            j=y
            i=x
            d=grid[y+dy][x+dx]

            if d == 0 or (dx == 0 and dy == 0):
                dx += 1
                continue
            while True:
                j += dy
                i += dx
                if (j < 0 or i < 0 or j >= HEIGHT
                                or i >= WIDTH or grid[j][i] == 0 and grid[j][i] is not None):
                    break
                d -= 1
                if d == 0:
                    break

            if d == 0:
                j=y
                i=x
                d=grid[y+dy][x+dx]

                ''' The next section chooses inverse-video    *
                * or not, then "walks" chosen valid     *
                * move, characters with mode '''

                if on:
                    stdscr.attron(A_STANDOUT)
                while d > 0:
                    j += dy
                    i += dx

                    if not on and has_colors():
                        newval = grid[j][i]
                        stdscr.addch(j, i, str(newval), attribs[newval-1])
                        stdscr.refresh()
                    else:
                        stdscr.addch(j, i, str(grid[j][i]))
                        stdscr.move(y, x)
                        stdscr.refresh()

                    d -= 1
                    if d == 0 :
                        break

                if on:
                    stdscr.attroff(A_STANDOUT)
            dx += 1
        dy += 1


def doputc(c):
    # may no longer be necessary
    ''' doputc() simply prints out a character to stdout, by tputs() '''
    print(c)


def topscores(newscore):
    '''
     * topscores() processes its argument with the high score file, any
     * updates to the file, outputs the list to the screen.  If "newscore"
     * is False, score file is printed to the screen (i.e. "greed -s")
    '''
    termbuf = tptr = boldon = boldoff = ""
    whoami = pwd.getpwuid(os.getuid())
    new = Score(whoami.pw_name, newscore)
    toplist = [new]

    ''' Catch all signals, high '''
    signal(SIGINT, SIG_IGN)
    ''' score file doesn't get     '''
    signal(SIGQUIT, SIG_IGN)
    ''' messed up with a kill.     '''
    signal(SIGTERM, SIG_IGN)
    signal(SIGHUP, SIG_IGN)

    # there was specific io c code here converted to be more pythonic
    if newscore > 0:
        # TODO Code smell 4: Extract method from comment
        ''' creates the file if it doesn't exist '''
        if not os.path.isfile(LOCALSCOREFILE):
            open(LOCALSCOREFILE, "a").close()
        with open(LOCALSCOREFILE, "r") as fd:
            for score_line in fd:
                score_elem = score_line.strip().split(";")
                toplist.append(Score(score_elem[0], int(score_elem[1]), score_elem[2]))
        toplist.sort(key=lambda s: s.score, reverse=True)
        with open(LOCALSCOREFILE, "a") as fd:
            fd.write("{};{};{}\n".format(new.username, new.score, new.time))

    if not toplist:
        ''' perhaps "greed -s" was run before *
        * any greed had been played? '''
        print("No high scores.")

    ''' print out list to screen, score, any '''
    for i, tmp_score in enumerate(toplist):
        timestr = tmp_score.time
        if tmp_score == new:
            print("\x1b[6;30;42m" + "{} {} {} {} {}".format(
                i+1, tmp_score.time, tmp_score.score,
                ((int(tmp_score.score) * 100) / (HEIGHT * WIDTH)),
                tmp_score.username) + "\x1b[0m")
        elif i < MAXSCORES :
            print("{} {} {} {} {}".format(
                i+1, tmp_score.time, tmp_score.score,
                ((int(tmp_score.score) * 100) / (HEIGHT * WIDTH)),
                tmp_score.username))


def lockit(on):
    '''
     * lockit() creates a file with mode 0 to serve as a lock file.  The creat()
     * call will fail if the file exists already, it was made with mode 0.
     * lockit() will wait approx. 15 seconds for the lock file, then
     * override it (shouldn't happen, might).  "on" says whether to turn
     * locking on or not.
     '''
    fd
    # Changed x to i
    i = 1

    if on:
        try:
            fd = open(LOCKPATH, O_RDWR | O_CREAT | O_EXCL, 0)
        except:
            print("error")

        while (fd < 0):
            print("Waiting for scorefile access... {}/15\n".format(i))
            i += 1
            if i >= 15:
                print("Overriding stale lock...")
                if unlink(LOCKPATH) == -1:
                    print("{}: {}: Can't unlink lock.\n".format(cmdname, LOCKPATH))
                sys.exit(1)
            time.sleep(1)
        close(fd)
    else:
        os.unlink(LOCKPATH)


def msg(helpwin, row, msg):
    helpwin.addstr(row, 2, msg)

def help():
    '''
     * help() simply creates a window over stdscr, writes the help info
     * inside it.  Uses macro msg() to save space.
     '''
    global helpwin
    global stdscr

    if not helpwin:
        helpwin = curses.newwin(18, 65, 1, 7)
        ''' print box around info '''
        helpwin.box(curses.ACS_VLINE, curses.ACS_HLINE)
        helpwin.addch(curses.ACS_ULCORNER)
        helpwin.addch(1, 30, curses.ACS_URCORNER)
        helpwin.addch(15, 0, curses.ACS_LLCORNER)
        helpwin.addch(15, 30, curses.ACS_LRCORNER)

        msg(helpwin, 3, " The object of Greed is to erase as much of the screen as")
        msg(helpwin, 4, " possible by moving around in a grid of numbers.  To move,")
        msg(helpwin, 5, " use the arrow keys, number pad, one of the letters")

        msg(helpwin, 7, " When you move in a direction, erase N number of grid")
        msg(helpwin, 8, " squares in that direction, being the first number in that")
        msg(helpwin, 9, " direction.  Your score reflects the total number of squares")
        msg(helpwin, 10," eaten.  Greed will not let you make a move that would have")
        msg(helpwin, 11," placed you off the grid or over a previously eaten square")
        msg(helpwin, 12," unless no valid moves exist, which case your game ends.")
        msg(helpwin, 13," Other Greed commands are 'Ctrl-L' to redraw the screen,")
        msg(helpwin, 14," 'p' to toggle the highlighting of the possible moves, and")
        msg(helpwin, 15," 'q' to quit.  Command line options to Greed are '-s' to")
        msg(helpwin, 16," output the high score file.")

        helpwin.move(17, 64)
        helpwin.refresh()
    else:
        stdscr.touchwin()
        helpwin.refresh()

    helpwin.getch()
    helpwin.touchwin()
    stdscr.refresh()


def signal_handler(signal, frame):
    global stdscr
    stdscr.move(23, 0)
    stdscr.refresh()
    endwin()
    ''' writes two newlines '''
    puts("\n")
    topscores(score)
    exit(0)

''' the end '''

def print_test(value):
    stdscr.addstr(
        23, 20,
        value
    )
    stdscr.move(y, x)
    stdscr.refresh()


if  __name__ == "__main__":
    main()
